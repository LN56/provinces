# 省市区三级联动（移动端）

#### 介绍
这个项目是一个移动端的省市区三级联动模块，是一个jQuery插件

#### 软件架构
软件架构说明


#### 在线预览
   演示地址：[http://www.leeny.top/provinces/](http://www.leeny.top/provinces/)

#### 使用说明

1. 调用方式：
```
	$(slecter).Picker();
```
2. 参数说明：
```
	{
		city: true,		//false 表示该列不显示
		area: true,  	//false 表示该列不显示
		pca: ['广东','广州','越秀'],	//默认省市区
		pick:function(res){},	// 确定之后回调
	}
```
> 注：默认省市区为数组形式，三个元素，元素内容不要加省市区的行政级别。可以为空数组，为默认北京、北京、东城区。
#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)